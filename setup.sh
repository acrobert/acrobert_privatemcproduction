vm
export SCRAM_ARCH=slc7_amd64_gcc700
source /cvmfs/cms.cern.ch/cmsset_default.sh

cd CMSSW_10_6_19_patch3/src
eval `scram runtime -sh`
scram b -j8
cd ../..
