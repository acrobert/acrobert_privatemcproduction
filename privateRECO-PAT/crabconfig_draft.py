from WMCore.Configuration import Configuration
config = Configuration()

config.section_("General")
config.General.requestName = "privateMCProduction_#WHOAMI#_#BASENAME#_stepMiniAODv2_#NEVENTS#evts_#REQUESTDATE#"
config.General.workArea = 'crab_privateMCProduction'
config.General.transferLogs = True

config.section_("JobType")
#config.JobType.pluginName = 'PrivateMC'
config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'customChain_stepRECO-PAT_cfg.py'
config.JobType.disableAutomaticOutputCollection = False
config.JobType.maxMemoryMB = 4000

config.section_("Data")
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 5
config.Data.totalUnits = #NUMBERFILES#
config.Data.publication = True
config.Data.inputDBS = 'phys03'
config.Data.ignoreLocality = True
config.Data.outputDatasetTag = 'eventMiniAODv2_#BASENAME#_#NEVENTS#evts_#REQUESTDATE#'
config.Data.outLFNDirBase = '/store/user/acrobert/mcprod/#BASENAME#/'
config.Data.inputDataset = '#INPUTDATASET#'


config.section_("Site")
config.Site.storageSite = 'T3_US_CMU'
config.Site.whitelist = ['T1_US_*','T2_US_*','T3_US_*']
#config.Site.whitelist = ['T2_*']

config.section_("User")
