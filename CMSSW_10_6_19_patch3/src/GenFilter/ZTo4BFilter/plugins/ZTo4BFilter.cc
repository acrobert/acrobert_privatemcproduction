#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/global/EDFilter.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "DataFormats/METReco/interface/BeamHaloSummary.h"

#include "GenFilter/ZTo4BFilter/interface/ZTo4BFilter.h"

// count number of q daughters that decay to not-b's
int countQDaughters( const reco::GenParticleRef part, int qpdg ) {
  int countQDaughters_rec(const reco::Candidate* part, std::vector<const reco::Candidate*>& qlist, int qpdg );
  std::vector<const reco::Candidate*> qlist;

  int nq = 0;
  for (unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    nq += countQDaughters_rec( part->daughter(i), qlist, qpdg );
  }

  return nq;
}

// function for recursion above (type needs to be Candidate)
int countQDaughters_rec( const reco::Candidate* part, std::vector<const reco::Candidate*>& qlist, int qpdg ) {

  bool hasqdaughter = false;
  int nq = 0;
  for ( unsigned int i = 0; i < part->numberOfDaughters(); i++) {
    nq += countQDaughters_rec( part->daughter(i), qlist, qpdg );
    
    if( abs(part->daughter(i)->pdgId()) == qpdg ) {
      hasqdaughter = true;
    }
  }

  if( abs(part->pdgId()) == qpdg && !hasqdaughter ) {
    bool bexists = false;
    for( const reco::Candidate* p : qlist ) {
      if( p->pt() == part->pt() && p->eta() == part->eta() && p->phi() == part->phi() ) bexists = true;
    }

    if( !bexists ) {
      qlist.push_back(part);
      nq += 1;
    }
  }

  return nq;
}


ZTo4BFilter::ZTo4BFilter(const edm::ParameterSet& iConfig) {
  genParticleCollectionT_ = consumes<reco::GenParticleCollection>(iConfig.getParameter<edm::InputTag>("genParticleCollection"));
  produces<bool>();
}

bool ZTo4BFilter::filter(edm::StreamID iID, edm::Event& iEvent, const edm::EventSetup& iSetup) const {
  
  edm::Handle<reco::GenParticleCollection> genParticles;
  iEvent.getByToken(genParticleCollectionT_, genParticles);

  //bool hasExtraQs = false;
  bool zto4b = false;
  int nu, nd, ns, nc, nb;
  for ( unsigned int iG = 0; iG < genParticles->size(); iG++ ) {
    reco::GenParticleRef iGen( genParticles, iG );

    if( iGen->pdgId() == 23 && iGen->status() == 62 ) {
      //nu = countQDaughters(iGen, 1);
      //nd = countQDaughters(iGen, 2);
      //ns = countQDaughters(iGen, 3);
      //nc = countQDaughters(iGen, 4);
      nb = countQDaughters(iGen, 5);

      //std::cout << "Z " << iG << " u " << nu << " d " << nd << " s " << ns << " c " << nc << " b " << nb << std::endl;

      //if (nu > 0 || nd > 0 || ns > 0 || nc > 0) hasExtraQs = true;
      if (nb == 4 || nb == 6 || nb == 8) zto4b = true; 
    }
  }

  if (zto4b) {
    std::cout << ">> Found Z-> 4B" << std::endl; 
    return true;
  } else {
    return false;
  }
  

  //if (hasExtraQs) {
  //  return true;
  //} else {
  //  std::cout << "Rejecting event" << std::endl;
  //  return false;
  //}

  //std::cout << "Rejecting event" << std::endl;
  //return false;
}

#include "FWCore/Framework/interface/MakerMacros.h"
DEFINE_FWK_MODULE(ZTo4BFilter);
