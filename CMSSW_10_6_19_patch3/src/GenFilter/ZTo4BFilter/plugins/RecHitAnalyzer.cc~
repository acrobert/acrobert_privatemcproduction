// --------------------------------------------
//
// Package:    PhotonClassifier
// Class:      RecHitAnalyzer
// 
//
// Author: Andrew C. Roberts
// Started 2022/5/18
// Last Updated 2022/5/18
//
// --------------------------------------------

#include "PhotonClassifier/RecHitAnalyzer/interface/RecHitAnalyzer.h"

//
// constructors and destructor
//
RecHitAnalyzer::RecHitAnalyzer(const edm::ParameterSet& iConfig) {

  usesResource("TFileService");
  edm::Service<TFileService> fs;  

  RHTree = fs->make<TTree>("RHTree", "RecHit tree");
  RHTree->Branch("eventId", &eventId_);
  RHTree->Branch("runId",   &runId_);
  RHTree->Branch("lumiId",  &lumiId_);

}

RecHitAnalyzer::~RecHitAnalyzer() {
}

//
// member functions
//
//
// ------------ method called for each event  ------------
void RecHitAnalyzer::analyze(const edm::Event& iEvent, const edm::EventSetup& iSetup) {

  using namespace edm;

  eventId_ = iEvent.id().event();
  runId_ = iEvent.id().run();
  lumiId_ = iEvent.id().luminosityBlock();

  RHTree->Fill();

}


//define this as a plug-in
DEFINE_FWK_MODULE(RecHitAnalyzer);
